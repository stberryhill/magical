#ifndef _TYLER_MAGICAL_H
#define _TYLER_MAGICAL_H

#include <math.h>

extern const double M_PI_OVER_180;

/* Fast approximations */
float m_FastInverseSquareRoot(float input);
float m_FastSquareRoot(float input);

/* Angle conversions */
float m_DegreesToRadians(float input);
float m_RadiansToDegrees(float input);

/* min/max float */
float m_MinFloat(float a, float b);
float m_MaxFloat(float a, float b);

/* Vector operations */
void  m_Vector3f_Add(const float a[], const float b[], float *output);
void  m_Vector3f_Subtract(const float a[], const float b[], float *output);
void  m_Vector3f_PiecewiseMultiply(const float a[], const float b[], float *output);
float m_Vector3f_DotProduct(const float a[], const float b[]);
float m_Vector3f_Magnitude(const float input[]);
void  m_Vector3f_Normalize(const float input[], float *output);
void  m_Vector3f_Scale(const float input[], const float scalar, float *output);
void  m_Vector3f_CrossProduct(const float a[], const float b[], float *output);
float m_Vector3f_AngleBetweenVectors(const float a[], const float b[]);

void  m_Vector2f_Add(const float a[], const float b[], float *output);
void  m_Vector2f_Subtract(const float a[], const float b[], float *output);
void  m_Vector2f_PiecewiseMultiply(const float a[], const float b[], float *output);
float m_Vector2f_DotProduct(const float a[], const float b[]);
float m_Vector2f_Magnitude(const float input[]);
void  m_Vector2f_Normalize(const float input[], float *output);
void  m_Vector2f_Scale(const float input[], const float scalar, float *output);
float m_Vector2f_AngleBetweenVectors(const float a[], const float b[]);
void m_Vector2f_AbsoluteValue(const float input[], float *output);

/* Matrix creation */
void m_Matrix4f_Identity(float *output);
void m_Matrix4f_PerspectiveProjection(const float fov, const float aspectRatio, const float near, const float far, float *output);
void m_Matrix4f_OrthographicProjection(const float left, const float right, const float top, const float bottom, const float near, const float far, float *output);
void m_Matrix4f_Translation(const float *translation3f, float *output);
void m_Matrix4f_Rotation(const float *rotation3f, float *output);
void m_Matrix4f_Scale(const float *scale3f, float *output);
const char *m_Matrix4f_ToString(const float *input);

/* Matrix operations */
void m_Matrix4f_Add(const float *a, const float *b, float *output);
void m_Matrix4f_Subtract(const float *a, const float *b, float *output);
void m_Matrix4f_Multiply(const float *a, const float *b, float *output);

/* Arbitrary, n x n matrix operations */
void m_MatrixNf_Identity(const int matrixSize, float *output);
void m_MatrixNf_Zeros(const int matrixSize, float *output);
void m_MatrixNf_Ones(const int matrixSize, float *output);
void m_MatrixNf_Add(const int matrixSize, const float *a, const float *b, float *output);
void m_MatrixNf_Subtract(const int matrixSize, const float *a, const float *b, float *output);
void m_MatrixNf_Multiply(const int matrixSize, const float *a, const float *b, float *output);
void m_MatrixNf_AddScalar(const int matrixSize, const float scalar, const float *inputMatrix, float *output);
void m_MatrixNf_SubtractScalar(const int matrixSize, const float scalar, const float *inputMatrix, float *output);
void m_MatrixNf_MultiplyScalar(const int matrixSize, const float scalar, const float *inputMatrix, float *output);
void m_MatrixNf_MultiplyPiecewise(const int matrixSize, const float *a, const float *b, float *output);
const char *m_MatrixNf_ToString(const int matrixSize, const float *input);

void m_MatrixMNf_Zeros(const int m, const int n, float *output);
void m_MatrixMNf_Ones(const int m, const int n, float *output);
void m_MatrixMNf_Add(const int m, const int n, const float *a, const float *b, float *output);
void m_MatrixMNf_Subtract(const int m, const int n, const float *a, const float *b, float *output);
void m_MatrixMNf_AddScalar(const int m, const int n, const float scalar, const float *inputMatrix, float *output);
void m_MatrixMNf_SubtractScalar(const int m, const int n, const float scalar, const float *inputMatrix, float *output);
void m_MatrixMNf_MultiplyScalar(const int m, const int n, const float scalar, const float *inputMatrix, float *output);
void m_MatrixMNf_MultiplyPiecewise(const int m, const int n, const float *a, const float *b, float *output);
void m_MatrixMNf_Cut(const int m, const int n, const float *input, const int width, const int stride, const int offset, float *output);
const char *m_MatrixMNf_ToString(const int m, const int n, const float *input);

#endif
