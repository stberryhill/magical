#Compiler to use
CC=gcc -std=c89 -Wall

#Include Directory(External Libraries, EX: OpenGL, GLEW)
SOURCEDIR = ./
OBJDIR = build
INCLUDE = ../

#Grab sources & headers
PISOURCES = $(wildcard $(SOURCEDIR)/*.c) #platform independent sources

HEADERS = $(wildcard $(SOURCEDIR)/*.h)

#Use addprefix function to add ./obj/ to each individual obj file name (EX: ./obj/ + Camera.o = ./obj/Camera.o)
OBJECTS = $(addprefix $(OBJDIR)/, $(notdir $(PISOURCES:.c=.o)))

#Compiler flags
CFLAGS= -Wall -g -I $(INCLUDE) -I ./

#Set executable name
STATICLIB=../libMagical.a
DYNAMICLIB=../libMagical.so

all: $(STATICLIB) $(OBJECTS)

$(OBJDIR)/%.o: $(SOURCEDIR)/%.c 
	$(CC) -c $< $(CFLAGS) -o $@


#Objects depend on object directory
$(OBJECTS): | $(OBJDIR)

#Create new object directory if one doesnt exist already
$(OBJDIR):
	mkdir $(OBJDIR)

.PHONY : clean
clean:
	rm $(STATICLIB) $(OBJECTS)

$(STATICLIB): $(OBJECTS)
	ar rcs $@ $^
	cp magical.h ../

$(DYNAMICLIB): $(OBJECTS)
	$(CC) -shared -o $@ $^