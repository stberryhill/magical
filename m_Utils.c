#include "magical.h"

const double M_PI_OVER_180 = M_PI / 180.0;

float m_DegreesToRadians(float input) {
  return input * M_PI_OVER_180;
}

float m_RadiansToDegrees(float input) {
  return input / M_PI_OVER_180;
}

float m_MinFloat(float a, float b) {
    return a < b ? a : b;
}
float m_MaxFloat(float a, float b) {
    return a > b ? a : b;
}