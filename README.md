# Magical
Minimal 3d math library in C89
by S. Tyler Berryhill


To be as minimalistic as possible, vectors and matrices are both 1-dimensional float arrays. This is slightly more memory efficient 
than using structs. Currently, there aren't even any typedefs you have to size the array correctly yourself, simply because I didn't
want it to be opaque what the matrices and arrays really are. This may change, the syntax is pretty clunky.

All functions are prefixed by *"m_"*, this is specifically to take advantage of autocompletion and make it easy to discover and
rediscover functions. I often forget exact spelling, or even what functions I've written so this makes it easy to "look it up" without
having to open the header file and browse around.